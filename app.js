// Load credentials
require('dotenv').config()

// Load modules
const express = require('express')
const createError = require('http-errors')
const favicon = require('serve-favicon')
const exphbs = require('express-handlebars')
const path = require('path')

// Routes
const frontendRoutes = require('./routes/frontend')
const backendRoutes = require('./routes/backend')

// Express
const app = express()
app.use(express.static(path.join(__dirname, 'public')))
app.use(favicon(path.join(__dirname, 'public/images', 'favicon.png')))
app.disable('x-powered-by')

const hbs = exphbs.create({
  extname: 'hbs',
  defaultLayout: 'main',
  helpers: require('./lib/helpers')
})

// Register `hbs.engine` with the Express app.
app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')

// Router
app.use(frontendRoutes())
app.use('/api', backendRoutes())

// Error Handler
app.use((req, res, next) => {
  next(createError(404))
})
// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.status = err.status || 500
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
