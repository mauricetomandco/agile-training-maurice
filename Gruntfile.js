// Load credentials
require('dotenv').config()

module.exports = grunt => {
  grunt.initConfig({
    shell: {
      coverage: {
        command: 'cross-env NODE_ENV=development nyc --reporter=text-summary mocha --reporter=list'
      },
      'coverage-push': {
        command: 'nyc report --reporter=text-lcov > .nyc_output/coverage.lcov && npx codecov'
      }
    }
  })

  grunt.loadNpmTasks('grunt-shell')

  grunt.registerTask('default', ['shell'])
}
