const { Router } = require('express')
const router = new Router()

// Controller
const backendController = require('../controllers/backend')

const backendRouter = () => {
  router.route('/').get(backendController.hackathon)
  return router
}

module.exports = backendRouter
