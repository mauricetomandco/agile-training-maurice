const { expect } = require('chai')
const request = require('supertest')

// App
const app = require('../app')
const agent = request.agent(app)

// Test suite
describe('API', () => {
  it('should say `Hello Hackathon`', async () => {
    const res = await request(app).get('/api')
    expect(res.status).to.equal(200)
    expect(res.text).to.be.a('string')
    expect(res.text).to.equal('Hello Hackathon')
  })

  it('should echo a query', done => {
    agent
      .get('/api')
      .query({ q: 'echo' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('echo')
        done()
      })
  })

  it('should echo my name', done => {
    agent
      .get('/api')
      .query({ q: '0000e550: what is your name' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('Maurycy')
        done()
      })
  })

  it('should compare numbers', done => {
    agent
      .get('/api')
      .query({ q: 'ed7c9c80: which of the following numbers is the largest: 679, 72, 802, 96' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('802')
        done()
      })
  })

  it('should add numbers', done => {
    agent
      .get('/api')
      .query({ q: '62bc2190: what is 14 plus 6' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('20')
        done()
      })
  })

  it('should add many numbers', done => {
    agent
      .get('/api')
      .query({ q: '7db1c900: what is 15 plus 5 plus 6' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('26')
        done()
      })
  })

  it('should minus numbers', done => {
    agent
      .get('/api')
      .query({ q: 'c158af70: what is 7 minus 4' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('3')
        done()
      })
  })

  it('should check square and cube (when none)', done => {
    agent
      .get('/api')
      .query({ q: '2ce131e0: which of the following numbers is both a square and a cube: 699, 1936' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('')
        done()
      })
  })

  it('should check square and cube (when any)', done => {
    agent
      .get('/api')
      .query({ q: 'd1c58cb0: which of the following numbers is both a square and a cube: 225, 881, 805, 64' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('64')
        done()
      })
  })

  it('should multiply', done => {
    agent
      .get('/api')
      .query({ q: '3178c8b0: what is 14 multiplied by 2' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('28')
        done()
      })
  })

  it('should know bond movies', done => {
    agent
      .get('/api')
      .query({ q: '3178c8b0: who played James Bond in the film Dr No' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('Sean Connery')
        done()
      })
  })

  it('should know geography', done => {
    agent
      .get('/api')
      .query({ q: '3178c8b0: which city is the Eiffel tower in' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('Paris')
        done()
      })
  })

  it('should know politics', done => {
    agent
      .get('/api')
      .query({ q: '3178c8b0: which year was Theresa May first elected as the Prime Minister of Great Britain' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('2016')
        done()
      })
  })

  it('should know fruits', done => {
    agent
      .get('/api')
      .query({ q: '3178c8b0: what colour is a banana' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('yellow')
        done()
      })
  })

  it('should find prime numbers', done => {
    agent
      .get('/api')
      .query({ q: '208639d0: which of the following numbers are primes: 337, 860' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('337')
        done()
      })
  })

  it('should know Fibonacci sequence', done => {
    agent
      .get('/api')
      .query({ q: 'd4a598a0: what is the 10th number in the Fibonacci sequence' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('55')
        done()
      })
  })

  it('should calc power', done => {
    agent
      .get('/api')
      .query({ q: '7f543f90: what is 4 to the power of 0' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('1')
        done()
      })
  })

  it('should calc compilcated things', done => {
    agent
      .get('/api')
      .query({ q: '3e769f10: what is 7 multiplied by 0 plus 4' })
      .expect('Content-Type', /text/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.text).to.be.a('string')
        expect(res.text).to.equal('4')
        done()
      })
  })

  context('should error', () => {
    it('on request to a non-existing endpoint', done => {
      agent
        .get('/api/bla')
        .expect(404)
        .end(done)
    })
  })
})
