module.exports = {
  hackathon: (req, res) => {
    let query = req.query
    let question = req.query.q
    let answer = question || 'Hello Hackathon'

    // Log a query from game server
    if (process.env.NODE_ENV != 'test') console.log(query) // eslint-disable-line no-console

    if (typeof question === 'string' && question.match(/what is your name/i)) {
      answer = 'Maurycy'
    }

    if (typeof question === 'string' && question.match(/which of the following numbers is the largest/i)) {
      const vars = question
        .split('largest:')[1]
        .split(',')
        .map(i => i.trim())
        .map(i => parseInt(i, 10))
        .filter(i => !!i)
      answer = Math.max(...vars)
    }

    if (typeof question === 'string' && question.match(/what is (\d+) plus (\d+)/i)) {
      const m = question.match(/what is (\d+) plus (\d+)/i)
      answer = parseInt(m[1], 10) + parseInt(m[2], 10)
    }

    if (typeof question === 'string' && question.match(/what is (\d+) plus (\d+) plus (\d+)/i)) {
      const m = question.match(/what is (\d+) plus (\d+) plus (\d+)/i)
      answer = parseInt(m[1], 10) + parseInt(m[2], 10) + parseInt(m[3], 10)
    }
    if (typeof question === 'string' && question.match(/what is (\d+) minus (\d+)/i)) {
      const m = question.match(/what is (\d+) minus (\d+)/i)
      answer = parseInt(m[1], 10) - parseInt(m[2], 10)
    }
    if (typeof question === 'string' && question.match(/what is \d+ to the power of \d+/i)) {
      const m = question.match(/what is (\d+) to the power of (\d+)/i)
      answer = Math.pow(parseInt(m[1], 10), parseInt(m[2], 10))
    }

    if (typeof question === 'string' && question.match(/which of the following numbers is both a square and a cube/i)) {
      answer =
        question
          .split('and a cube:')[1]
          .split(',')
          .map(i => i.trim())
          .map(i => parseInt(i, 10))
          .filter(i => !!i)
          .find(i => Math.sqrt(i) % 1 < 0.000001 && Math.cbrt(i) % 1 < 0.000001) || ''
    }

    if (typeof question === 'string' && question.match(/what is \d+ multiplied by \d+/i)) {
      const m = question.match(/what is (\d+) multiplied by (\d+)/i)
      answer = parseInt(m[1], 10) * parseInt(m[2], 10)
    }

    if (typeof question === 'string' && question.match(/what is the \d.* number in the Fibonacci sequence/i)) {
      const m = question.match(/what is the (\d+).* number in the Fibonacci sequence/i)
      answer = fib(parseInt(m[1], 10))
    }

    if (typeof question === 'string' && question.match(/which of the following numbers are primes/i)) {
      answer = question
        .split('primes:')[1]
        .split(',')
        .map(i => i.trim())
        .map(i => parseInt(i, 10))
        .filter(i => !!i)
        .find(test_prime)
    }

    if (typeof question === 'string' && question.match(/who played James Bond in the film Dr No/i)) {
      answer = 'Sean Connery'
    }

    if (typeof question === 'string' && question.match(/which city is the Eiffel tower in/i)) {
      answer = 'Paris'
    }

    if (typeof question === 'string' && question.match(/what colour is a banana/i)) {
      answer = 'yellow'
    }

    if (
      typeof question === 'string' &&
      question.match(/which year was Theresa May first elected as the Prime Minister of Great Britain/i)
    ) {
      answer = '2016'
    }

    if (
      typeof question === 'string' &&
      question.match(/what is \d+ (plus|minus|multiplied by) \d+ (plus|minus|multiplied by) \d+/i)
    ) {
      const m = question.match(/what is (\d+) (plus|minus|multiplied by) (\d+) (plus|minus|multiplied by) (\d+)/i)
      const a = parseInt(m[1], 10)
      const op1 = m[2]
      const b = parseInt(m[3], 10)
      const op2 = m[4]
      const c = parseInt(m[5], 10)

      let res
      if (op1 === 'multiplied by') {
        res = a * b
        if (op2 === 'plus') {
          res += c
        } else if (op2 === 'minus') {
          res -= c
        } else if (op2 === 'multiplied by') {
          res *= c
        }
      } else if (op2 === 'multiplied by') {
        res = b * c
        if (op1 === 'plus') {
          res += a
        } else if (op1 === 'minus') {
          res = a - res
        } else if (op1 === 'multiplied by') {
          res *= c
        }
      } else if (op2 === 'plus') {
        res = b + c
        if (op1 === 'plus') {
          res += a
        } else if (op1 === 'minus') {
          res = a - res
        } else if (op1 === 'multiplied by') {
          res *= c
        }
      }

      answer = res
    }

    // Answer the question
    res.status(200).send(String(answer))
  }
}

function test_prime(n) {
  if (n === 1) {
    return false
  } else if (n === 2) {
    return true
  } else {
    for (var x = 2; x < n; x++) {
      if (n % x === 0) {
        return false
      }
    }
    return true
  }
}

function fib(n) {
  let arr = [0, 1]
  for (let i = 2; i < n + 1; i++) {
    arr.push(arr[i - 2] + arr[i - 1])
  }
  return arr[n]
}
